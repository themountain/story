# story
  Story is a series of short conversation-based stories I write during my Unemployment.

# This project is growing.

The first look at the stories might not convey much meaning as it does not have character names mentioned in stories while others only have emotions added beside the prompts. But we humans tend to create meaning out of such meaningless situations sometimes.

If you got anything meaningful out of these stories, Please consider creating an Issue. I would love to hear more about it. 🙂


# Contribute
### Have any exciting stories of your own to tell?
I have created a separate branch for anyone who would like to contribute to this.

### Format
If you have not figured it out by now,
Each story in Matrix explains the story's name(file name) to some extent.
If you write multiple stories, The addition of those file names also exerts some meaning in the form of commit messages.

You can write stories in any language as long as it is meaningful to you. 

Please consider creating a pull request on the दुनिया(World) branch.

Thank you.

-- The End.
