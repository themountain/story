    "Mother's Birthday" said the Calendar.
    "Ahh! It has been a year already." said Akshay with sadness in his tone.

--------* 1 Year later
    
    "Mother's Birthday" said the Calendar.
    
    "It has been two years." said Akshay with a hint of grief.

--------* 2 years later
    
    "Mother's Birthday" said the Calendar.
    
    "How the time flies." said Akshay resting on his bed.

--------* 3 years later
    
    "Mother's Birthday." said the Calendar.
    
    "Akshay is out playing Football. Please remind later." said the AI personal assistant.

-- The End.