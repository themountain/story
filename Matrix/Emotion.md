    "Why are you so afraid of showing your emotions?" said Anuj with disgust.

    "Because they kill." Dev replied ferociously.

    "Better to die with them. Than to be alive without them." Anuj told his father with utter disappointment.

-- The End.