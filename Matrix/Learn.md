    “How many pages did you read today?”

    “I don’t know. Maybe around 60.”

    “Ooohh!! You are going to finish that book in another 2 days with that rate.”

    “Yeah. I might complete it tomorrow itself.”

—————* 1 day later

    “So, How was the book.”

    “It was great.”

    “Really. What was it all about?”

    “I don’t know. I forgot.”

    “What? Must have been a waste of time. No?”

    “Naah! I liked how it was written.”

— The End.