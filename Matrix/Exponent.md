    "It's called Chess my lord." concluded the Brahmin as he finished explaining the game.

    "I am grateful to have received such an intricate gift. What would you like in return? Ask anything and you shall receive." said the kind delighted.

    "You are very kind my lord. I come from a very small village and we don't harvest enough food to sustain throughout the year. If your highness approves, I would ask for only one grain of wheat for the first square of the chess board and to double it till we reach the 64th box."

    "Hah! That's all." laughed the king ignoring the exponential behaviour of the situation.

-- The End.