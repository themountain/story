    "It is a summation ofcourse."

    "Summation of what?"

    "Everything. Since you were born till you die. From your first observation to your last contribution."

    "Does that mean I am contributing to a culture by asking this question?" asked the student hesitantly.

    "Yes. In fact this culture will have a shred of your hesitance too." replied his Sister calmly. 

-- The End.