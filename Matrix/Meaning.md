    "What do you mean by Culture?" H asked O with Enthusiasm.

    "It's when we humans come together and build something." O replied with confidence.

    "Makes sense." H a little less enthusiastic and more shocked.

Though he confirmed that both of them were on the same page about the Meaning of the word. There were inconsistencies on how they approached the problem.

------------* 4 months later

    "I don't think we are communicating on the same level. Thus, I am not fit to perform my duties to the best of my abilities. I have to quit." said H with a bittersweet feeling in their heart.

-- The End.