    "In some cases, It used to signify borders of early days kingdoms. Since these hills were visible from afar and the flags on top of these temples would signify the ruling King's કુળદેવી."

    "Ahh! It's so late already. I think we should leave. Thank you again for such a wonderful dinner Ankita."

    "Are you sure you want to leave at such a late hour? Stay the night and leave tomorrow if you like." said Ajay insisting.

    "Thank you so much for the offer. But I need to leave for Bangalore tomorrow and need to finish some work before that."

    "Okay. I won't come between you and your work." said Ajay snickering. "It would be better to take the highway at such a late hour. It will take a bit longer to reach your hotel, But it will be safer." He said while Jitesh was putting on his helmet.

While on his rental bike, Jitesh thought of what Ajay meant by highway being "safer". Meanwhile he also crossed a temple that he had not seen before. Concerned that he might have missed the highway he looked up the route to his hotel in Google Maps.

    "5 Minutes." said Google Maps.

    "Ahh! Seems like I missed the highway. But this route will take only 5 minutes." thought Jitesh ignoring Ajay's advice.

--------* 2 months later.

    "Hello Everyone,
    
    I have a sad news to share today.
    
    I dreamt of creating a network so large that no other Indian would ever feel a need to buy a bike. But while investing these 15 years to make this dream a reality, I guess I missed few clear indicators from the market.
    
    It feels like there is a monster lurking behind the shadows, Eating out this network without sharing enough details on what is happening to our bikes. 2 months ago we lost our last bike which would make us unable to pay our debts and file for insolvency.
    
    I am here today to thank each and every one of you for investing your time and efforts to make this company what it was.There are some steps we are taking to help you out in this hard time as much as possible...." read Ajay in his email from the CEO of the company he worked for.

-- The End.