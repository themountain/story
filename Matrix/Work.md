The scene was set. The fireplace was lit, lights were dimmed.

    "Which movie do you want to watch today?" asked the excited girlfriend.

    "IDK. What about LOR?" replied the boyfriend bit sheepishly.

    "We have watched that 3 times already. Let's watch Jury instead."

    "Ahh! Okay."

-----* 30 mins later.

    "Whaaat!! That's crazy." exclaimed the girlfriend on the plot twist of the movie.

Astonished by not getting the same reaction from their partner, She looked at him...

    "Dude!! Are you working right now?" she pinged him on his holodeck as not to disturb him.

    "Yeah! Haha.. I am sorry but my brain started thinking about work like 20 minutes ago and then the chip suggested to switch to the Holodeck. You know how it goes."

    "You are unreal Mr. Pusk. Unreal I say." replied the agitated girlfriend. 

-- The End.