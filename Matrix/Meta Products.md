    "I have spent last 2 months binging on reels. And since I am student of science, I look at all my activities from an expermential lense."

    "Sounds interesting. Please go on!!"

    "So, One thing that I have noticed is that people do crazy things to get attention."

    "Not a surprise. It is very difficult to gain attention. And more difficult it is to hold that attention."

    "I agree. You need something more. Something new, Every single time. That draws and keeps the attention of a specific subset of people. You are going to have difficult time figuring it out or you are going to be really good at it. And this will drive your chances of survival on such platfortm."

    "You got all of this from reels?"

    "It's not just that. The platform itself is also an entity in this equation. So, The platform wants to keep you into the system as long as possible, Because house always needs to win. And to win house first needs participants. That is why platforms need to invoke mechanisms in place that does not let people leave the platform."

    "What do you mean?"

    "Try, marking every single thing that you get on your feeds as not relevant. That would mean you are not happy with the content that the system has generated. And see the quality(Theme, Dialogues, Emotions) of reels you get. You should mark enough posts as not relevant so that the system thinks you might leave if you will not get enough relevant content."

    "I have to try this... I will let you know."

-- The End.