    "Hey, What is your grandfather's name? The one who worked crazy hours."
  
    "Randhir?" Ajay said raising his head from the ground.
  
    "Ooh, And what does it mean?" said Akshya with mild curiosity.
  
    "It means Brave." Ajay replied persistently.

------* 2 days ago.

    "Dad, What does Grandpa's name mean?"
  
    "उज्जवल".


Stories are crazy. What is even more crazier is the format in which they are shared.
Like sharing a secret code of a new adventure. You can never grasp, Where it will lead you to. But you can definitely learn a lot from them.

    "randhir0106@29"
    
    "Incorrect password. Your account has been locked." said the ATM.
    
    "Ahhhhhh!" cried Akshay and pulled the trigger.

At which incorrect attempt did Akshay pull the trigger?

-- The End.