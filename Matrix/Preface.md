    "I feel like I always talk in metaphors. Because I find it really difficult to explain the thing that I want to with the vocabulary that I possess.

    Anyway! This book is an attempt to create an example that can explain the meaningless nature of the world that we live in while still finding meaningful interactions everywhere we go. A microcosm of the real world if I may say.

    I have tried my best to structure it in a way that could truly explain the idea that I was running with. But to be honest, I did not have it planned when I started out with the project." Author wrote.

-- The End.