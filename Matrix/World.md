    "Excuse me. I seem to have lost my way to my hostel."

    "何?" replied the woman hinting she does not know any English.

    "I don't have internet right now. And I seem to have lost my way to my hostel." Nico tried hinting towards his phone to explain the situation to her in some way.

    "ああ！ わかった。" she pulled out her phone and opened Google maps.

Nico entered the location of his hostel to see which turn he had missed in this maze of Barcelona streets. After realizing which turn he had to make, He thanked the kind hearted lady before taking his leave.

    "Hey!" said the rector suspiciously.

    "Hey. Do you know where Pablo is?"

    "He left. His shift completed."

    "Ahh okay."

    "You are staying with us?"

    "Yes. Just for today." Nico said pulling his keys out from his bag.

    "You should submit the keys at the reception before leaving."

    "Ohh! I didn't know. But what about my stuff? I thought it would be great if I could hang on to my keys."

    "No. No. You should keep your keys at the reception. We should be able to investigate your room or your things if the government officials come."

    "And why would they come?" asked Nico curiously.

    "In case they suspect any terorist activities."

    "Okay. But I am leaving to catch my flight anyway." replied Nico with a slight imperativeness. 

-- The End.